/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculatormvc;

/**
 *
 * @author Lydia
 */
public class CalculatorModel {
    private int result;
    public void calculate(int x , int y, String s){
        if(s == "Add"){
            result = add(x, y);
        }
        else if(s == "Subtract"){
            result = substract(x, y);
        }
        else if(s == "Multiply"){
            result = multiply(x, y);
        }
        else if(s == "Divide"){
            result = divide(x, y);
        }
    }
    public int add(int x , int y)
    {
        return x+y;
    }
    
    public int substract(int x , int y)
    {
        return x-y;
    }
     public int multiply(int x , int y)
    {
        return x*y;
    }
      public int divide(int x , int y)
    {
        return x/y;
    }
    
    public int getResult()
    {
    return result;
    }
}
