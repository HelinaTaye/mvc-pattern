/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculatormvc;

import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Lydia
 */
public class CalculatorView extends JFrame{
    
        private JLabel num1 = new JLabel("First Number");
        private JTextField fn = new JTextField(5);
        private JLabel num2 = new JLabel("Second Number");
        private JTextField sn = new JTextField(5);
        String[] actionList = {"Add", "Subtract", "Multiply", "Divide"};
        JComboBox box = new JComboBox(actionList);
        private JTextField sol = new JTextField(5);
        
        public CalculatorView()
    {
        JPanel jp = new JPanel();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(600,300);
        jp.add(num1);
        jp.add(fn);
        jp.add(num2);
        jp.add(sn);
        jp.add(box);
        jp.add(sol);
        this.add(jp);
         }
    
    public int getFirstnu()
    {
        return Integer.parseInt(fn.getText());
    }
    public int getSectnu()
    {
        return Integer.parseInt(sn.getText());
    }
    
     public JComboBox getComboBox()
    {
        return box;
    }
    
    public void setSolu(int solution)
    {
        sol.setText(Integer.toString(solution));
    }
    void addClaculationListner(ActionListener forButton)
    {
        box.addActionListener(forButton);
    }
    void displayerror(String error)
    {
        JOptionPane.showMessageDialog(this, error); }
    }
    
