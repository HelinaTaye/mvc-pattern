/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculatormvc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;

/**
 *
 * @author Lydia
 */
public class CalculatorController {
    private CalculatorView theView;
    private CalculatorModel theModel;
    
    public CalculatorController (CalculatorView theView, CalculatorModel theModel)
    {   
        this.theView = theView;
        this.theModel = theModel;
        theView.addClaculationListner(new CalculateListener());
    }
    
    
    class CalculateListener implements ActionListener
    {
    public void actionPerformed(ActionEvent e)
    {
    int firstNumber, secondNumber = 0;
    try{
        firstNumber = theView.getFirstnu();
        secondNumber = theView.getSectnu();
        JComboBox box = theView.getComboBox();
        String s =(String) box.getSelectedItem();
        theModel.calculate(firstNumber, secondNumber, s);
        theView.setSolu(theModel.getResult());
    }
    catch(NumberFormatException ex)
    {
        System.out.println(ex);
        theView.displayerror("You Need to Enter 2 Integers"); } 
    }
    }
}

